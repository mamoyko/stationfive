// Lib imports
import Vue from 'vue'
import Vuex from 'vuex'

//Modules imports
import foods from "./modules/foods";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  getters: {},
  actions: {},
  mutations: {},
  modules: {
    foods
  }
})