const state = {
    food : null,
    key : null,
    notification : null
 }
 
 const getters = {

    isGroup1Valid : ( state, getters, rootState, rootGetters ) => payload => {

        let pattern = null
        let data = payload.data
        let item = payload.item

        switch(item) {
            case '101':
                pattern = ["201","202","206","302"]
                break
            case '102':
                pattern = ["201", "301"]
                break
            case '103':
                pattern = ["202"]
                break
            case '204':
                pattern = ["304"]
                break
            case '205':
                pattern = ["304"]
                break
            default : pattern = null
        }

        let match =  getters["isMatch"]({ data : data, pattern : pattern })
        return match
    },

    isMatch : ( state, getters, rootState, rootGetters ) => payload => {
        let data = payload.data
        let pattern = payload.pattern
        if (pattern === null){
            return true
        }
        let match = data.map((item) => {
            return pattern.includes(item)
        })
        return match.every((item) => item === false)
    },

    seachArr : ( state, getters, rootState, rootGetters) => payload => {

        let arr = state.food
        let tempArr = []

        for (let i = 0 ; i < arr.length; i++){
            let innerArrayLength = arr[i].length;
            for (let j = 0 ; j < innerArrayLength; j++){
                if (payload === arr[i][j].id){
                    return arr[i][j]
                }
            }
        }
    }
 }
 
 const actions = {

    setSelectedFood({ dispatch, commit, state, rootState, getters, rootGetters }, payload){
        return new Promise((resolve,reject) => {
            
            let group

            if ( state.key === 1 ) {
                group = getters["isGroup1Valid"]({data : payload, item : payload[0]})
            } else if ( state.key === 2 ) {
                group = getters["isGroup1Valid"]({data : payload, item : payload[1]})
            } else {
                group = true
            }
    
            resolve(group)
        })
    },

    showFoodData({ dispatch, commit, state, rootState, getters, rootGetters }, payload){
        return new Promise((resolve,reject) => {
           let data = payload.map(getters["seachArr"])
           commit('setNotification', data)
           resolve(data)
        })
    }
     
 }
 
 const mutations = {

    setFoodData(state, payload){
        state.food = payload
    },

    setKey(state,payload){
        state.key = payload
    },

    setNotification(state,payload){
        state.notification = payload
    }
 }
 
 export default {
     namespaced: true,
     state,
     getters,
     actions,
     mutations
 }