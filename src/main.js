import Vue from 'vue'
import App from './App.vue'
import router from './router'

import store from '@/store'

import 'bootstrap/dist/css/bootstrap.css'

let foodData = fetch('./data.json').then(resp => { return resp.json() })

let requests = [foodData]

Promise.all(requests)
  .then((results) => {

    for (let key in results){
      if (results[key] == undefined){
        console.error('config error')
      }
    }

    const foodDatajson = results[0]
    store.commit('foods/setFoodData', foodDatajson)
    
  })

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
